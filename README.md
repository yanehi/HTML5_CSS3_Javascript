# Was findet Ihr in diesem Repository?
- Informationen zu HTML, CSS3 und Javascript
- Einfache Beispiele zum erstellen einer eigenen Webseite
- Code Snippeds
- Links zu Tutorials, Refernzen und Bücher mit denen ich selbst gearbeitet habe 
- Kurze Erklärung zu Gitlab und wie man es mithilfe von [Tortoise GIT](https://tortoisegit.org/) und der Kommandozeile benutzt

# Benutzte Entwicklungsumgebung
- [Webstorm](https://www.jetbrains.com/products.html?fromMenu#type=ide)

# Benutzter Browser
- [Google Chrome](http://www.chip.de/downloads/Google-Chrome-32-Bit_32709574.html)

# Quellenangaben - YouTube Channels
- [Simplex](https://www.youtube.com/playlist?list=PLHIolF3q4faHtPmfTPzk8kiK7N3FvjFKj)
- [Derek Banas](https://www.youtube.com/watch?v=fju9ii8YsGs&index=3&list=FL1j0wMIC0b3VIDhDcB0fdHQ)
- [htmlworld](https://www.youtube.com/watch?v=7wJw2V0z_9k&list=PLUgMKRb1K9gBOtkAYQKhIOHk4MqHYDqOr)


# Quellenangaben - Bücher
- [Javascript-Die universelle Sprache zur Web-Programmierung](https://drive.google.com/file/d/0B9UW_JtTGlphckdyRV80WDRLNXM/view?usp=sharing)
- [Webseiten Programmierung](https://drive.google.com/file/d/0B9UW_JtTGlphRC1QNDFDNVlZUDA/view?usp=sharing)

# Referenzen
- [HTML5/CSS3](https://drive.google.com/file/d/0B9UW_JtTGlphdWE4a2lNZ2RZLUU/view?usp=sharing)

# Einrichtung von GIT
- ???
- weitere Infos im Wiki
